/*! \file
\brief Define symbolic constants and debug messages

*/
# define RAD_2_DEG  57.296 ;

#define VERSION "V 4"
#define IS_PILOT 1
#define IS_SCIENCE 2
#define IS_CONFIG 3

# define Yaw_axis 0
# define Pitch_axis 1
# define Roll_axis 2

# define X_axis 0
# define Y_axis 1
# define Z_axis 2

#define MOT_H_AV 0
#define MOT_H_AR 1
#define MOT_V_AV 2
#define MOT_V_AR 3
#define MOT_L_TR 4
#define MOT_L_AN 5

// Some debug message
#ifdef FREE_RAM
	#define PRINT_FREE_RAM \
	Serial.print(F("RAM libre dans \""));   Serial.print(__FILE__); \
	Serial.print(F("\" - ")); Serial.print(__LINE__); \
	Serial.print(F(" --> "));Serial.println(freeRam());
#else
	#define PRINT_FREE_RAM
#endif

#ifdef DEBUG
//
	#define DEBUG_MESSAGE_SERVER_1 \
	Serial.print(F("Server 1 is at ")); \
	Serial.print(Ethernet.localIP()); Serial.print (" port : "); Serial.println(PORT_PILOT) ; \
	Serial.print(F("Server 2 is at ")); \
	Serial.print(Ethernet.localIP()); Serial.print (" port : "); Serial.println(PORT_SCIENCE) ;
//
	#define DEBUG_MESSAGE_INIT_I2C Serial.println(F("Initializing I2C devices..."));
//
	#define DEBUG_MESSAGE_TEST_MPU \
	Serial.println(F("Testing MPU connections...")); \
   	Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
//
	#define DEBUG_MESSAGE_INIT_DMP Serial.println(F("Initializing DMP..."));
//
	#define DEBUG_MESSAGE_ENABLE_DMP Serial.println(F("Enabling DMP..."));
//
        #define DEBUG_MESSAGE_ENABLE_INT Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)...")); 
//
        #define DEBUG_MESSAGE_DMP_READY	Serial.println(F("DMP ready! Waiting for first interrupt..."));
//
       	// ERROR 1 = initial memory load failed / 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
	#define DEBUG_MESSAGE_DMP_FAIL Serial.print(F("DMP Initialization failed (code ")); \
	Serial.print(IMU.devStatus); Serial.println(F(")"));
//
	#define DEBUG_MESSAGE_INIT_CARD Serial.println(F("Init des sorties cartes objet : CARD_OUTPUTS"));
//
	#define DEBUG_MESSAGE_FIN_SETUP Serial.print(F("Memoire libre 0 -- > ")) ;Serial.println(freeRam()); \
	Serial.print(F("VERSION 6"));
//
	#define DEBUG_MESSAGE_BMP_085_CAL Serial.println("calibration du BMP 085");
//

	#define DEBUG_MESSAGE_CONFIG	Serial.println (F("on cause au configurateur"));\
		Serial.println (F("Voila sa requette :"));\
		Serial.println(buffer); 
#else
	#define DEBUG_MESSAGE_SERVER_1 Serial.print(F("Serveurs en route"));
	#define DEBUG_MESSAGE_INIT_I2C
	#define DEBUG_MESSAGE_TEST_MPU
	#define DEBUG_MESSAGE_INIT_DMP
	#define DEBUG_MESSAGE_ENABLE_DMP
	#define DEBUG_MESSAGE_ENABLE_INT
 	#define DEBUG_MESSAGE_DMP_READY
	#define DEBUG_MESSAGE_DMP_FAIL
	#define DEBUG_MESSAGE_INIT_CARD
	#define DEBUG_MESSAGE_FIN_SETUP Serial.print(F("FIN SETUP Version 6"));
	#define DEBUG_MESSAGE_BMP_085_CAL
	#define DEBUG_MESSAGE_CONFIG
#endif
//

