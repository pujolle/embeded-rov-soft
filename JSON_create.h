/*! \file
\brief Class for creating a JSON formated string to send ROV datas to surface station

\todo Clean unused fonctions

*/
# ifndef JSON_CREATE_H
# define JSON_CREATE_H

#define JSON_MAX_LENGTH 350


class JSON_REQUETTE{
public :
	char JSON_OUT[JSON_MAX_LENGTH] ;
	JSON_REQUETTE(){
                I = 12 ;
		END = &JSON_OUT[0];
		*END ++ ='{'; 
		*END  ='\0'; 
	} ;
//	void add_array_name(char name[]);
//	void add_array_data(int N);
//	void add_array_data(float N);
	void add_key_name(char name[]);
	void add_key_data(int N);
	void add_key_data(float N);
	void close();
	void clean();
	char AA1[12];
	char * END ;
	char * AUX ;
	int I ; // pointe sur l'avant dernier char, juste avant '\0'
private :

};

void JSON_REQUETTE::close(){ 
	*END ++ = '}' ; *END  ='\0'; 
	// strconcat(JSON_OUT, "}" ) ; 
}
void JSON_REQUETTE::clean(){ 
		END = &JSON_OUT[0];
		*END++ ='{'; 
		*END  ='\0';
}

void JSON_REQUETTE::add_key_name(char name[]){
        *END -- ;
	if ( *END != '{' ){ 
		*END++; *END++ =',';
	}  else {
          *END ++ ;
        }
	*END ++ ='\"';
	while (*name != '\0'){ *END ++ = *name ++ ;}
	*END ++ = '\"' ; 
	*END ++ = ':' ; 
	*END  = '\0' ;
}
void JSON_REQUETTE::add_key_data(int N){
	itoa (N,AA1,10) ;
	AUX = &AA1[0];
	while (*AUX != '\0'){ *END ++ = *AUX ++ ;}
	*END ++ = '.' ;
	*END ++ ='0' ;	
	*END  = '\0' ; 
}
void JSON_REQUETTE::add_key_data(float F){
	ftoa (AA1 , F) ; 
	AUX = &AA1[0];
	while (*AUX != '\0'){ *END ++ = *AUX ++ ;}
}


/*
void JSON_REQUETTE::add_array_name(char name[]){
	if ( !JSON_OUT[I] == '{' ){ 
		I++; JSON_OUT[I] =',';
	}  
	I++; JSON_OUT[I] ='\"';
	while (*name != '\0'){ I++ ; JSON_OUT[I] = *name ++ ;}
	I++; JSON_OUT[I] = '\"' ; 
	I++; JSON_OUT[I] = ':' ; 
	JSON_OUT[I+1] = '\0' ;
}

void JSON_REQUETTE::add_array_data(int N){
	if ( JSON_OUT[I] == ':' ){
		I++; JSON_OUT[I] ='[';
		I++; JSON_OUT[I]='\0';
	} 
	if ( JSON_OUT[I] == ']' ){
	 	JSON_OUT[I] =',';
		I++; JSON_OUT[I]='\0';
	} 
	itoa (N,AA1,10) ;
	byte J = 0 ;
	while (AA1[J] != '\0'){ I++ ; J++ ; JSON_OUT[I] = AA1[J] ;}
	I++; JSON_OUT[I]='.' ;
	I++; JSON_OUT[I]='0' ;	
	I++; JSON_OUT[I]=']' ;	
	JSON_OUT[I+1] = '\0' ;
}
void JSON_REQUETTE::add_array_data(float F){
	if ( JSON_OUT[I] == ':' ){
		I++; JSON_OUT[I] ='[';
		I++; JSON_OUT[I]='\0';
	} 
	if ( JSON_OUT[I] == ']' ){
	 	JSON_OUT[I] =',';
		I++ ; JSON_OUT[I]='\0';
	} 
	ftoa (AA1 , F) ; 
	byte J = 0 ;	
	while (AA1[J] != '\0'){ I++ ; J++ ; JSON_OUT[I] = AA1[J] ;}
	I++; JSON_OUT[I]=']' ;	
	JSON_OUT[I+1] = '\0' ;
}
*/

# endif


