#ifndef UTILS_H
#define UTILS_H

void ftoa (
/*! \param[out] : buffer receiving output string */ 
char buffer[] , 
/*! \param[in] : Float to convert*/
float F){
/*!
 Transform a float to a string with 3 digit after decimal point

*/

	if (isnan(F)){
	buffer[0] = 'N' ;	buffer[1] = 'a' ;	buffer[2] = 'N' ;	buffer[3] = '\0' ;
	return ;
	}
	if (isinf(F)) {
	buffer[0] = 'I' ;	buffer[1] = 'n' ;	buffer[2] = 'f' ;	buffer[3] = '\0' ;
	return ;
	}
	long I ; int i = 0;
	I = 1000*F ;
	ltoa(I,buffer,10);
	while (buffer[i] != '\0'){i++;}
	//
	if (buffer[0] =='-'){
		if (i == 4 ){	
			buffer[6] = '\0'; 
			buffer[5] = buffer[3] ;
			buffer[4] = buffer[2] ;
			buffer[3] = buffer[1] ;
			buffer[2] = '.' ;
			buffer[1] = '0' ;
			return ;
		}
		if (i == 3){	
			buffer[6] = '\0'; 
			buffer[5] = buffer[2] ; 
			buffer[4] = buffer[1] ; 
			buffer[3] = '0' ; 
			buffer[2] = '.' ;
			buffer[1] = '0' ;
			return ;
		} 
		if (i == 2){	
			buffer[6] = '\0'; 
			buffer[5] = buffer[1] ;
			buffer[4] = '0' ; 
			buffer[3] = '0' ; 
			buffer[2] = '.' ;
			buffer[1] = '0' ; 
			return ;
		} 
	}
	if (i == 3){	
		i+=1 ;
		buffer[i+1] = '\0'; 
		buffer[i] = buffer[i-2] ; i-- ;
		buffer[i] = buffer[i-2] ; i-- ;
		buffer[i] = buffer[i-2] ; i-- ;
		buffer[i] = '.' 	; i-- ;
		buffer[i] = '0' 	; i-- ;	
		return ;
	} 
	if (i == 2){	
		i+=2 ;
		buffer[i+1] = '\0'; 
		buffer[i] = buffer[i-3] ; i-- ;
		buffer[i] = buffer[i-3] ; i-- ;
		buffer[i] = '0' 	; i-- ;	
		buffer[i] = '.' 	; i-- ;
		buffer[i] = '0' 	; i-- ;	
		return ;
	} 
	if (i == 1){	
		i+=3 ;
		buffer[i+1] = '\0'; 
		buffer[i] = buffer[i-4] ; i-- ;
		buffer[i] = '0' 	; i-- ;	
		buffer[i] = '0' 	; i-- ;	
		buffer[i] = '.' 	; i-- ;
		buffer[i] = '0' 	; i-- ;	
		return ;
	} 
		buffer[i+1] = '\0'; 
		buffer[i] = buffer[i-1] ; i-- ;
		buffer[i] = buffer[i-1] ; i-- ;
		buffer[i] = buffer[i-1] ; i-- ;
		buffer[i] = '.' ;
}

void strconcat(char *S1, char *S2){
	while (*S1 != '\0'){ *S1++; } ; 
	while (*S2 != '\0'){ *S1++ = *S2++ ;} 
       *S1 = '\0';
}

int freeRam ()
{
    extern int __heap_start, *__brkval;
    int v;
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}


#endif

