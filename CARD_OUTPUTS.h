/*! \file

\brief Class for computing orders to motors and hardware from pilot commands

* Orders are computed from pilot commands in several ways, depending of PILOT_COMMANDS::PilotMode.<br />
* Only mode 0 is implemented : direct command, simple mix of order on each axes.

*/

#ifndef CARD_OUTPUTS_H
#define CARD_OUTPUTS_H

#include "ROV.h"
#include "PILOT_COMMAND.h"
#include "IMU.h"
#include "configurateur.h"
#include <Servo.h> 
/*!
\brief Class for computing orders to motors and hardware from pilot commands

* Orders are computed from pilot commands in several ways, depending of PILOT_COMMANDS::PilotMode.<br />
\warning Only mode 0 is implemented : direct command, simple mix of order on each axes.

*/
 class CARD_OUTPUTS{
 public :
	/*! 0 --> Look downward 180 --> look upward max, probably the servo wont use so large tilt angle, gain could be added*/
	byte Tilt_Cam_angle ;
	/*! Commands to motor's ESC Calculated from pilot's commands */
	byte MOTOR_COMMANDE[6] ; 
	/*! Value on PWM output for led driver control \todo not programmed yet, choose an output and test interface to led drivers*/
	byte Analog_led_power ; 
	/*! Servo object created to command the camera tilt servo */
	Servo Servo_Tilt_Cam ;
	/*! Store the prop motor level of power Incremented when pilot push on right Analog trigger, decremented when push left trigger
	\todo : this management should be displaced in the surface station JAVA prog and X axis would be treated as a normal axis */
	float MOTOR_POWER ;
	void init();
	void Auto_pilot(PILOT_COMMANDS * PC, IMU_VARS * IMU ,FORM_MOT * MOT, FORM_PID * PID , byte * PLUG_TO_MOT) ;
	void Affiche_commandes();
	void Affiche_commandes_court();
private :
	void send_serial(byte * PLUG_TO_MOT);
	void send_serial_stop();
} ;
/*! 
\brief Receive commands from surface station, and transform them to order for motors

Implement very basic calculation of motors power from pilot commands <br />

Send commands on serial port 1 to central housing card <br />
use PLUG_TO_MOT to send the command in the right order to send the right command to the right motor

\todo implement gain control, filtering, stabilisation, use infos from IMU to filter 
human pilot command. 

*/
void CARD_OUTPUTS::Auto_pilot(
/*! \param[in] Pilot command */
PILOT_COMMANDS * PC, 
/*! \param[in] Datas from IMU*/
IMU_VARS * IMU ,
/*! \param[in] Pointer on table storing motors limits*/ 
FORM_MOT * MOT,  
/*! \param[in] pointer on table of PID coeff */
FORM_PID * PID , 
/*! \param[in] Pointer on table associating a command to a plug */
byte * PLUG_TO_MOT){
	int E[3], S[3] ;

	for (int i = 0 ; i < 3 ; i++){
		// Addition des trim au ordres, les trims comptent pour 10 %, arbitraire peut changer
		E[i]  =  PC->E[i] +0.1*PC->TE[i]; 
		S[i]  =  PC->S[i] +0.1*PC->TS[i]; 
	}
	// Si mode Accel / frein
	MOTOR_POWER += ((PC->A + 90)/50.0) - ((PC->B + 90)/20.0) ; S[X_axis] = MOTOR_POWER ;
	//
	MOTOR_COMMANDE[MOT_L_TR] =  	90 + MOT[MOT_L_TR].sens*( E[Roll_axis]  + S[X_axis]) ;
	MOTOR_COMMANDE[MOT_L_AN] =  	90 + MOT[MOT_L_AN].sens*(-E[Roll_axis]  + S[X_axis]) ;
	MOTOR_COMMANDE[MOT_H_AV] = 	90 + MOT[MOT_H_AV].sens*( E[Yaw_axis]   + S[Y_axis]) ;
	MOTOR_COMMANDE[MOT_H_AR] = 	90 + MOT[MOT_H_AR].sens*(-E[Yaw_axis]   + S[Y_axis]) ;
	MOTOR_COMMANDE[MOT_V_AV] = 	90 + MOT[MOT_V_AV].sens*(-E[Pitch_axis] + S[Z_axis]) ;
	MOTOR_COMMANDE[MOT_V_AR] = 	90 + MOT[MOT_V_AR].sens*( E[Pitch_axis] + S[Z_axis]) ;	
	// OUTPUTS		
	Servo_Tilt_Cam.write(Tilt_Cam_angle); 
	// send datas through serial port 1 to central housing
	send_serial(PLUG_TO_MOT) ;
}
/*!
\brief Attach camera servo to Pin 3 and send STOP command to all motors
*/
void CARD_OUTPUTS::init(){
 	Servo_Tilt_Cam.attach(3); 
	MOTOR_POWER = 0.0 ;
	for (int i = 0 ; i < 6 ; i++){  MOTOR_COMMANDE[i] =  	89  ; }
	send_serial_stop();
 }
/*!
\brief Send commands on serial port 1 to central housing card

send the command in the right order to send the right command to the right motor
*/
 void CARD_OUTPUTS::send_serial(
/*! \param[in] Table associating the index of the plug (as key) and the index of the motor (as value)*/
byte * PLUG_TO_MOT){

 	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[0]])	; Serial1.print(","); // PIN 3
	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[1]])	; Serial1.print(","); // PIN 9
	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[2]])	; Serial1.print(","); // PIN 10
	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[3]])	; Serial1.print(","); // PIN 11
	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[4]])	; Serial1.print(","); // PIN 12
	Serial1.print(MOTOR_COMMANDE[PLUG_TO_MOT[5]])	; Serial1.print("\n"); // PIN 13

 }
/*!
\brief Stop all motors

Don't care of the order
*/
  void CARD_OUTPUTS::send_serial_stop(){

 	Serial1.print(89)	; Serial1.print(",");
	Serial1.print(89)	; Serial1.print(",");
	Serial1.print(89)	; Serial1.print(",");
	Serial1.print(89)	; Serial1.print(",");
	Serial1.print(89)	; Serial1.print(",");
	Serial1.print(89)	; Serial1.print("\n");

 }
 
 // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
 //                 DEBOGAGE, AFFICHAGE SUR MONITEUR SERIE
 // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
/*! 
\brief debogage util : send on serial port 0 values of commands send to motors
*/
void CARD_OUTPUTS::Affiche_commandes_court(){
	Serial.print ("Hor "); Serial.print(MOTOR_COMMANDE[MOT_H_AV]); Serial.print ("/"); Serial.print(MOTOR_COMMANDE[MOT_H_AR]); 
	Serial.print ("\tVer "); Serial.print(MOTOR_COMMANDE[MOT_V_AV]); Serial.print ("/"); Serial.print(MOTOR_COMMANDE[MOT_V_AR]); 
	Serial.print ("\tLon "); Serial.print(MOTOR_COMMANDE[MOT_L_TR]); Serial.print ("/"); Serial.print(MOTOR_COMMANDE[MOT_L_AN]); Serial.println(); 
}
/*! 
\brief debogage util : Send on serial port a graphical representation of commands to motors
*/
void affiche_moteur(byte angle){
	byte i ;
	if (angle > 90){
		for (i= 0 ; i < 12 ; i++){ Serial.print(" ");} Serial.print("|");
			for (i= 0 ; i < (angle-90)/10 ; i++){
				Serial.print("-");
			}
			Serial.print(">");
	}
	if (angle < 90){
		for (i= 0 ; i < 11 - (90-angle)/10  ; i++){ Serial.print(" ");}
		Serial.print("<");
		for (i= 0 ; i < (90-angle)/10 ; i++){
				Serial.print("-");
		}
		Serial.print("|");
	}
	if (angle == 90){
		for (i= 0 ; i < 12 ; i++){ Serial.print(" ");} Serial.print("|");
	}
}
/*! 
\brief debogage util : send on serial port 0 values of commands send to motors (verbose)
*/
void CARD_OUTPUTS::Affiche_commandes(){
	Serial.print ("Horizontal AV = "); Serial.print(MOTOR_COMMANDE[MOT_H_AV]); 
	Serial.print (" AR = "); Serial.print(MOTOR_COMMANDE[MOT_H_AR]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_H_AV]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_H_AR]); Serial.println(); 
	Serial.print ("Vertical AV = "); Serial.print(MOTOR_COMMANDE[MOT_V_AV]); 
	Serial.print (" AR = "); Serial.print(MOTOR_COMMANDE[MOT_V_AR]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_V_AV]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_V_AR]); Serial.println(); 
	Serial.print ("Longi trigo = "); Serial.print(MOTOR_COMMANDE[MOT_L_TR]); 
	Serial.print (" Anti trigo = "); Serial.print(MOTOR_COMMANDE[MOT_L_AN]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_L_TR]); Serial.println(); 
	affiche_moteur(MOTOR_COMMANDE[MOT_L_AN]); Serial.println(); 
}




#endif

