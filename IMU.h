/*! \file
\brief Class for managing mpu6050 IMU. 



// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class using DMP (MotionApps v2.0)
// 6/21/2012 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
// Changelog:
//     2012-06-21 - added note about Arduino 1.0.1 + Leonardo compatibility error
//     2012-06-20 - improved FIFO overflow handling and simplified read process
//     2012-06-19 - completely rearranged DMP initialization code and simplification
//     2012-06-13 - pull gyro and accel data from FIFO packet instead of reading directly
//     2012-06-09 - fix broken FIFO read sequence and change interrupt detection to RISING
//     2012-06-05 - add gravity-compensated initial reference frame acceleration output
//                - add 3D math helper file to DMP6 example sketch
//                - add Euler output and Yaw/Pitch/Roll output formats
//     2012-06-04 - remove accel offset clearing for better results (thanks Sungon Lee)
//     2012-06-01 - fixed gyro sensitivity to be 2000 deg/sec instead of 250
//     2012-05-30 - basic DMP initialization working

\bug Depend on a problematic interruption <br />
   NOTE: In addition to connection 3.3v, GND, SDA, and SCL, this sketch
   depends on the MPU-6050's INT pin being connected to the Arduino's
   external interrupt #0 pin. On the Arduino Uno and Mega 2560, this is
   digital I/O pin 2.
   On the Multiwii this pin is a normal output

\todo change bib or bib usage to be idependant of arduino INT 0 

\todo check for the latest version of the bib

*/



#ifndef IMU_H
#define IMU_H

/*! \brief Class for reading mpu6050 */
class IMU_VARS{
public :
	int ax, ay, az;
	int gx, gy, gz;
	int mx, my, mz;
	float heading, pitch, roll ;
	bool dmpReady ; // set true if DMP init was successful
	uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
	uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
	uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
	uint16_t fifoCount;     // count of all bytes currently in FIFO
	uint8_t fifoBuffer[64]; // FIFO storage buffer
	// orientation/motion vars
	Quaternion q;           // [w, x, y, z]         quaternion container
	VectorInt16 aa;         // [x, y, z]            accel sensor measurements
	VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
	VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
	VectorFloat gravity;    // [x, y, z]            gravity vector
	float euler[3];         // [psi, theta, phi]    Euler angle container
	float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
	//
	IMU_VARS(){
		dmpReady = false;
	} ;
	void read_fifo(bool mpuInterrupt, MPU6050 mpu) ;
private :
} ;

// =========================================================================================== //
void IMU_VARS::read_fifo(bool mpuInterrupt, MPU6050 mpu)
{
  		// reset interrupt flag and get INT_STATUS byte
    	mpuInterrupt = false;
  		mpuIntStatus = mpu.getIntStatus();
		// get current FIFO count
	    fifoCount = mpu.getFIFOCount();
		// check for overflow (this should never happen unless our code is too inefficient)
    		if ((mpuIntStatus & 0x10) || fifoCount == 1024) 
			{
			// reset so we can continue cleanly
        		mpu.resetFIFO();
        		Serial.println(F("FIFO overflow!"));
    			// otherwise, check for DMP data ready interrupt (this should happen frequently)
    		}else if (mpuIntStatus & 0x02) { 
				//Serial.println("ready to read MPU");
				// wait for correct available data length, should be a VERY short wait
				while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
				// read a packet from FIFO
				mpu.getFIFOBytes(fifoBuffer, packetSize);
				// track FIFO count here in case there is > 1 packet available
				// (this lets us immediately read more without waiting for an interrupt)
				fifoCount =0 ;//fifoCount -= packetSize;
				mpu.resetFIFO();	
				// display Euler angles in degrees
				mpu.dmpGetQuaternion(&q, fifoBuffer);
				mpu.dmpGetGravity(&gravity, &q);
				mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
				heading = ypr[0] * 180/M_PI ;
				pitch = ypr[1] * 180/M_PI ;
				roll = ypr[2] * 180/M_PI ;
			}	
}
// =========================================================================================== //




#endif

