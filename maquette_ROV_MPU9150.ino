/*! \file 
\version 4

\brief FRONT HOUSING multiwii main programm

\main TRUC


MAIN PROGRAM
Maquette de carte ROV 
Communique avec les clients 0.0 de l'univ d'orleans version 
Simule grossierement les mvt du ROV

Broche reserves : 0 et 1 : port serie / USB (name  Serial)
broche 2 : interruption de l'IMU 9150 Probleme la Multiwii ne prevoit pas ca du tout
broche 10 Chip Select (Slave select) du module ethernet
connecteurs dédiés : port serie  caisson AV <--> caisson CENTRAL (name Serial1) 
connecteurs dédiés : bus I2C 
Hard : 6 servos sur pin 3, 4, 5, 6 ,7 , 8  de la carte du caisson central
MPU6050 sur la multiwii megapirate
Atmega 2560 sur la multiwii
Shield ethernet sur minimodule

Carte multiwii SE reliée à la carte principale par liaison série.

Fourni deux serveur sur port "PORT_PILOT" et "PORT_SCIENCE" (obligatoirement différents)) avec l'IP "IP_ADDRESS"

accepte deux connections, decode une chaine d'octets 
et envoie les angles de commande des ESC vers la carte secondaire en liaison serie.
Lit les angles d'Euler de l'IMU 6050 et les envoie toutes les 1/10 de sec vers le client (serveur push) sous forme JSON

lib speciales : 
--> mpu6050 I2Cdev
// I2Cdev library collection - MPU6050 I2C device class
// Based on InvenSense MPU-6050 register map document rev. 2.0, 5/19/2011 (RM-MPU-6000A-00)
// 8/24/2011 by Jeff Rowberg <jeff@rowberg.net>
--> ArduinoJsonParser
//malloc-free JSON parser for Arduino
// Benoit Blanchon 2014 - MIT License

lib standard : 
wire
String
Serial
ethernet 
*/


// Uncomment to use internal simulator instead of real sensor
//# define SIMULE  
// Uncomment to have message about ram usage
// # define FREE_RAM
// Uncomment to have more verbous output on serial port
# define DEBUG
/*! SERVER IP ADDRESS --> use , (comma) and not . (dot)*/
# define IP_ADDRESS 192,168,0,40
/*! Due to limitation in arduino bib PILOT_PORT need to be different of SCIENCE_PORT*/
# define PORT_PILOT 9999
# define PORT_SCIENCE 9998

#include "config.h"
#include <math.h>
/*#include <JsonArray.h>*/
/*#include <JsonHashTable.h>*/
/*#include <JsonObjectBase.h>*/
/*#include <JsonParser.h>*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//#
// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#include "Wire.h"
// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"
//#include "MPU6050.h"
#include "MPU6050_6Axis_MotionApps20.h"
// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
#include "utils.h"
#include <EEPROM.h>
#include <Servo.h> 
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "ROV.h"
#include "PILOT_COMMAND.h"
#include "IMU.h"
#include "CARD_OUTPUTS.h"
#include "ROV_SENSORS.h"
#include "JSON_create.h"
#include "configurateur.h"
#define MAX_BUFF_LENGHT 250

/*  Double web server : accept on connection for pilot and one other for science monitor. */
//
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(IP_ADDRESS);
/*! Eth server for PILOT client */
EthernetServer Server_0(9999) ;
/*! Eth server for SCIENCE client */
EthernetServer Server_1(9998) ;
/*! Eth clients */
EthernetClient client[2] ;
/*! table storing  type of client IS_PILOT, IS_SCIENCE, IS_CONFIG */
byte Client_type[2] ;
//
#ifndef SIMULE
MPU6050 mpu;
 /*! Indique le passage à 1 de la broche d'interruption 0 de l'atmega \bug Non branchée sur la multiwii, mais le prog marche quand même a partir du moment ou on declenche la première lecture en toucant avec le doigt la broche 2... */
volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
/*! \todo LA carte multiwwi ne porte pas un BMP085 A CHANGER */
BMP085 AV_Psens ; 
#endif
/*! Order extracted from request from pilot station */
PILOT_COMMANDS PC ;
/*! Command (angular) to ESC, (PWM) to dimmer, ON/OFF to relay */
CARD_OUTPUTS CO ;
/*! Datas for mpu6050 */
IMU_VARS IMU ;
/*! dats from ROV sensors */
ROV_DATAS ROV ;
/*! JSON request to send to surface station*/
JSON_REQUETTE JS ;

FORM_MOT MOT[6] ;
FORM_PID PID[3] ;
 /*! table associating motor # (value) from plu # (key) */
byte PLUG_MAP[6] ; 
/*! will store last time data was sent to client */
long previousMillis = 0; 
/*! interval at which resend data for client  (50 ms / 20 times/sec)   */ 
const long interval = 50;  
/*! Var associated with simulation of ROV */         
long debut_inter = 0 ,fin_inter = 0 , duree_inter = 0 ;
/*! Buffer for incoming Eth request*/
char buffer[MAX_BUFF_LENGHT];

#ifndef SIMULE
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
void dmpDataReady() {
    mpuInterrupt = true;
}
#endif
//==============================================================================================================//
//                     ARDUINO SETUP										//
//==============================================================================================================//
void setup() {
#ifndef SIMULE
	Serial1.begin(115200);
#endif
	Serial.begin(115200); // pour des raisons obscure 9600 fait planter l'init du MPU6050
  	// start the Ethernet connection and the server:
	delay(1000);
	Serial.println(F("PROUT"));
	Ethernet.begin(mac, ip);
	Server_0.begin(); Server_1.begin();
	DEBUG_MESSAGE_SERVER_1
#ifndef SIMULE
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	//                       Init de l'imu 								//
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	// join I2C bus (I2Cdev library doesn't do this automatically)
	Wire.begin();  
	delay(100);				
	DEBUG_MESSAGE_INIT_I2C  // initialize device
	mpu.initialize(); 			
	DEBUG_MESSAGE_TEST_MPU
	/*load and configure the DMP */ 	
	DEBUG_MESSAGE_INIT_DMP
    	IMU.devStatus = mpu.dmpInitialize();
    	// make sure it worked (returns 0 if so)
    	if (IMU.devStatus == 0) {
		DEBUG_MESSAGE_ENABLE_DMP
        	mpu.setDMPEnabled(true);
 		DEBUG_MESSAGE_ENABLE_INT         	
		/*! \bug enable Arduino interrupt detection TRES SUSPECT la pate D2 de l'atmel de la multiwii n'est pas connectée à l'IMU et ça marche pourtant  */
        	attachInterrupt(0, dmpDataReady, RISING);
        	IMU.mpuIntStatus = mpu.getIntStatus() ;
        	DEBUG_MESSAGE_DMP_READY // set our DMP Ready flag so the main loop() function knows it's okay to use it
        	IMU.dmpReady = true;
        	// get expected DMP packet size for later comparison
        	IMU.packetSize = mpu.dmpGetFIFOPacketSize();
	} else {
		DEBUG_MESSAGE_DMP_FAIL	
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	//                       Init  du capteur de pression						//
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	/*! AV_Psens.Calibration(); \todo RETIRE EN ATTENDANT LA BIB POUR LE BON CAPTEUR  */

#endif
	DEBUG_MESSAGE_INIT_CARD // init des entrees sorties
	CO.init();
	Client_type[0] = Client_type[1] = false ;
	read_stocked_param(MOT, PID, PLUG_MAP) ; // lit les reglages (mappage des moteurs et gain des PID) stockees en EEPROM, les passe en memoire vive
	DEBUG_MESSAGE_FIN_SETUP
}
//==============================================================================================================//
//                     			ARDUINO LOOP 								//
//==============================================================================================================//
void loop() {
	int i ;
	Connect_client(0);
	Connect_client(1);
	// Lit les ordres de pilotages envoyes par la station surface
	if (Client_type[0] == IS_PILOT && client[0].available() ) 
	{
		PC.lire_request(client[0]); 	// --> PILOT_COMMAND.h
		CO.Auto_pilot(&PC, &IMU, MOT, PID, PLUG_MAP); // Transmet les ordres aux moteurs
		CO.Affiche_commandes_court();
	}
#ifndef SIMULE
	// Lit les donnees transmise par le caisson central sur port serie
	while (Serial1.available() > 0) 
	{
		ROV.Temp_CEN = Serial1.parseInt(); ROV.Pressure_CEN = Serial1.parseInt(); ROV.flooding_alarm_CEN = Serial1.parseInt();
		if (Serial1.read() == '\n') {
			while (Serial1.available() > 0) Serial1.read() ; // vide ce qui traine dans le buffer
			break ;
		}
	}
 	// EN ATTENDANT LE CAPTEUR DE PROFONDEUR
	/*! \todo Placer ici la lecture du 89BSD */
	ROV.depth = 175 ;
 	// lit les composantes de mvt sur l'IMU TODO : corriger le problème d'interruption.
	if(mpuInterrupt || IMU.fifoCount > IMU.packetSize)
	{  
		IMU.read_fifo(mpuInterrupt, mpu) ;
	}
#else
	// SIMULATION DU ROV SI ABSENCE DE CAPTEUR
	fin_inter = millis(); duree_inter = fin_inter - debut_inter ;
	if (duree_inter > 10){
		debut_inter = millis();
		ROV.Simule_ROV(duree_inter , &CO) ;
	}
#endif
	// renvoi des donnees vers la surface toutes les 100 ms
	unsigned long currentMillis = millis();      
	if(currentMillis - previousMillis > interval) { previousMillis = currentMillis;
#ifndef SIMULE
		ROV.heading = IMU.heading ; ROV.pitch = IMU.pitch ; ROV.roll = IMU.roll ;
		//ROV.Temp_FRO = AV_Psens.GetTemperature(); ROV.Pressure_FRO = AV_Psens.GetPressure(); 
 		// RETIRE EN ATTENDANT LA BIB POUR LE BON CAPTEUR TODO
		ROV.Temp_FRO = 23; ROV.Pressure_FRO = 890; // Renvoi des données arbitraires faute de Bib
#endif
		create_JSON_UP();
		if (Client_type[0] == IS_PILOT 	&& 	client[0].connected())	client[0].print(JS.JSON_OUT); 
		if (Client_type[1] == IS_SCIENCE && 	client[1].connected()) 	client[1].print(JS.JSON_OUT);
		JS.clean();	
	}
	// REnvoi le formulaire de configuration de la carte elec du ROV
	// Test a revoir: il n'y a pas lieu d'imposer que le configuratuer soit en client 1
	if (Client_type[1] == IS_CONFIG && client[1].connected())
	{
		// DEBUG_MESSAGE_CONFIG
		read_config_param(buffer, MOT, PID) ; // Lit la requette envoye par le formulaire (si il y en a une), stocke les reglage en EEPROM
		read_stocked_param(MOT, PID, PLUG_MAP) ; // lit les reglages (mappage des moteurs et gain des PID) stockees en EEPROM, les passe en memoire vive
		write_config_form(&client[1], MOT, PID); // envoie le form de reglage (prerempli avec les reglages actuels)
	}
}
//==============================================================================================================//
//                     		FIN ARDUINO LOOP 								//
//==============================================================================================================//
void Connect_client( byte n){
int i ;
	if (!Client_type[n]) {
  	    if (n == 0)	client[0] = Server_0.available(); // listen for incoming clients  
    	    if (n == 1)	client[1] = Server_1.available(); 
	} else {
		if (!client[n].connected()) {
			// close the connection:
    			client[n].flush(); client[n].stop();
    			Serial.print("client ");Serial.print( int(n));Serial.println(" disonnected");
			Client_type[n] = false ;
		}	
	}
	//++++++++++++++++//
	// identifie nouveau clients
 	 if (client[n] && !Client_type[n]){ 
		Serial.print(F("new client "));Serial.println( int(n));
		i = 0 ; memset(buffer , 0 , sizeof(buffer));
		while (client[n].available() && i < MAX_BUFF_LENGHT) {
			char c = client[n].read();
			buffer[i] = c; /* Serial.print(c); */
			i++ ;
		}
		buffer[i+1] = '\0' ;
		client[n].flush();

		if( strstr(buffer, "Pilot") != NULL ){
			Serial.print(F("CLIENT "));Serial.print( int(n));Serial.println(F(" en tant que PILOTE")); Client_type[n] = IS_PILOT ;
		} 
		else if (strstr(buffer, "Config") != NULL)
		{
			Serial.print(F("CLIENT "));Serial.print( int(n));Serial.println(F(" en tant que CONFIG"));Client_type[n] = IS_CONFIG ;
		}
		 else if (strstr(buffer, "Science") != NULL) 
		{
			Serial.print(F("CLIENT "));Serial.print( int(n));Serial.println(F(" en tant que SCIENCE")); Client_type[n] = IS_SCIENCE ;
		} else {
			Serial.print(F("CLIENT "));Serial.print( int(n));Serial.println(F(" est NON IDENTIFIE"));
    			client[n].stop();
    			Serial.println(F("Connection refusee"));
			Client_type[n] = false ;
		}
	}
}

// ==================================================================== //  
void create_JSON_UP(){
		JS.add_key_name("Er"); JS.add_key_data(ROV.roll ); 
		JS.add_key_name("Ep"); JS.add_key_data(ROV.pitch ); 	
		JS.add_key_name("Ey"); JS.add_key_data(ROV.heading); 				
		JS.add_key_name("Dp"); JS.add_key_data(ROV.depth);
		JS.add_key_name("PV"); JS.add_key_data(float(ROV.Pressure_FRO));
		JS.add_key_name("TV"); JS.add_key_data(ROV.Temp_FRO);	 
		JS.add_key_name("FV"); JS.add_key_data(ROV.flooding_alarm_FRO); 
		JS.add_key_name("PC"); JS.add_key_data(float(ROV.Pressure_CEN));				
		JS.add_key_name("TC"); JS.add_key_data(ROV.Temp_CEN);	
		JS.add_key_name("FC"); JS.add_key_data(ROV.flooding_alarm_CEN); 
		JS.add_key_name("POWER"); JS.add_key_data(CO.MOTOR_POWER); 
		JS.close();
}
// ==================================================================== //  



