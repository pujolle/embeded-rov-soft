/*! \file
\brief When called by a browser on <IP_ADDRESS>:9998?Config send a web page to configure physical ROV config 

*(associate plug to motor) and tune PID coeff
* write in EEPROM config parameters
*/

#ifndef CONFIGURATEUR_H
#define CONFIGURATEUR_H

#define print_HTTP_HEADER \
		client->println(F("HTTP/1.1 200 OK"));   \
		client->println(F("Content-Type: text/html"));  \
		client->println();   
//
#define print_HTML_START client->println(F("<html><head></head><body>")); 
#define print_HTML_END client->println(F("</body></html>")); 
/*! \brief Store config param for each motor */
struct FORM_MOT {
	/*! Text to print on the user interface form */
	char * texte ;
	/*! ID of the form field associated to motor */
	char * id ;
	/*! Index of the central housing plug where the moror is plugged */
	char plug ;
	/*! Motor sens  + mean that motor produce positive thrust when receiving a positive command see ROV axes*/
	char sens ;
	/*! Motor positive limit of power */
	char limP ;
	/*! Motor neagtive limit of power */
	char limM ;
} ;
/*! \brief Store config param for PID on each  6 axes */
struct FORM_PID {
	/*! Text to print on the user interface form */
	char * texte ;
	/*! ID of the form field associated to motor */
	char * id ;
	/*! Prop coeff of PID */
	char P ;
	/*! Integral coeff of PID */
	char I ;
	/*! differential coeff of PID */
	char D ;
	char Nz ;
} ;

char *PLUG_TEXT[]={	"A - Pin#11",
			"B - Pin#5",
			"C - Pin#6",
			"D - Pin#8",
			"E - Pin#12",
			"F - Pin#13"		
		};
/*! 
\brief Read form fields send by the user navigator and write them in EEPROM

*/
void read_config_param(char * buffer, FORM_MOT * MOT, FORM_PID * PID){
	int addr = 0 ;
	char * PS ;	
	if (strstr(buffer, "Param") != NULL){
		PS = strtok(buffer , "=I") ;
		for(int j = 0 ; j < 6 ; j++){
			PS = strtok(NULL , "I");
			//Serial.println(PS);
			MOT[j].plug = atoi(PS);
			PS = strtok(NULL , "I");
			MOT[j].sens = atoi(PS);
			PS = strtok(NULL , "I");
			MOT[j].limP = atoi(PS);
			PS = strtok(NULL , "I");
			MOT[j].limM = atoi(PS);
		}
		for(int j = 0 ; j < 3 ; j++){
			PS = strtok(NULL , "I");
			PID[j].P = atoi(PS);
			PS = strtok(NULL , "I");
			PID[j].I = atoi(PS);
			PS = strtok(NULL , "I");
			PID[j].D = atoi(PS);
		}
		for(int j = 0 ; j < 6 ; j++){
			EEPROM.write(addr, MOT[j].plug); addr ++ ;
			EEPROM.write(addr, MOT[j].sens); addr ++ ;
			EEPROM.write(addr, MOT[j].limP); addr ++ ;
			EEPROM.write(addr, MOT[j].limM); addr ++ ;
		}
		for(int j = 0 ; j < 3 ; j++){
			EEPROM.write(addr, PID[j].P); addr ++ ;
			EEPROM.write(addr, PID[j].I); addr ++ ;
			EEPROM.write(addr, PID[j].D); addr ++ ;
		}
	}
}
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
// Read in EEPROM values of parameters and prepar for writing
// in the form served to the user 
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
void read_stocked_param(FORM_MOT * MOT, FORM_PID * PID, byte * PLUG_TO_MOT){
	MOT[MOT_H_AV].texte  = "thruster HORIZONTAL AVANT" ;
	MOT[MOT_H_AR].texte  = "thruster HORIZONTAL ARRIERE" ;
	MOT[MOT_V_AV].texte  = "thruster VERTICAL AVANT" ;
	MOT[MOT_V_AR].texte  = "thruster VERTICAL ARRIERE" ;
	MOT[MOT_L_TR].texte  = "Propulseur TRIGO" ;
	MOT[MOT_L_AN].texte  = "Propulseur ANTI TRIGO" ;

	MOT[MOT_H_AV].id   = "Hv" ;
	MOT[MOT_H_AR].id   = "Hr" ;
	MOT[MOT_V_AV].id   = "Vv" ;
	MOT[MOT_V_AR].id   = "Vr" ;
	MOT[MOT_L_TR].id   = "Pt" ;
	MOT[MOT_L_AN].id   = "Pa" ;
	
	PID[0].texte = "X/roll" ;
	PID[1].texte = "Y/pitch" ;
	PID[2].texte = "Z/yaw" ;
	PID[0].id = "X";
	PID[1].id = "Y";
	PID[2].id = "Z";

	int addr = 0 ;
	for(int j = 0 ; j < 6 ; j++){
		MOT[j].plug = EEPROM.read(addr); addr ++ ;
		MOT[j].sens = EEPROM.read(addr); addr ++ ;
		MOT[j].limP = EEPROM.read(addr); addr ++ ;
		MOT[j].limM = EEPROM.read(addr); addr ++ ;
	}
	for(int j = 0 ; j < 3 ; j++){
		PID[j].P = EEPROM.read(addr); addr ++ ;
		PID[j].I = EEPROM.read(addr); addr ++ ;
		PID[j].D = EEPROM.read(addr); addr ++ ;
	}
	for (int j = 0 ; j < 6 ; j++){
		PLUG_TO_MOT[MOT[j].plug] = j ;
	}
//	Serial.println("Map des prises");
//	for(int j = 0 ; j < 6 ; j++){
//		Serial.print("Prise # ");Serial.print(j); Serial.print(" -- "); Serial.print(PLUG_TEXT[j]);
//		Serial.print(" <--> ");
//		Serial.print("Moteur # "); Serial.print(PLUG_TO_MOT[j]);Serial.print(" -- ");Serial.println(MOT[PLUG_TO_MOT[j]].texte);
//	 }	
}

void write_config_form(EthernetClient * client,  FORM_MOT * MOT, FORM_PID * PID){
	print_HTTP_HEADER
	client->println(F("<html><head>"));


	client->println(F("\
<script>\
function toto(){ \
	oForm = document.forms[0]; \
	oForm[\"Param\"].value =\"\" ; \
	var a = \"\"; \
	for (i = 0 ; i < oForm.length -2 ; i++ ){ \
		a += oForm.elements[i].value + \"I\" ; \
	} \
	alert(\"Ecriture en EEPROM des parametres\"); \
	oForm[\"Param\"].value = a ; \
	oForm.submit(); \
} \
</script>\
"));

	client->println(F("</head><body>"));  	
	client->print(F("<h1> Version de logiciel de la carte ROV : "));client->print(VERSION); client->println(F("</h1>"));  			
	client->println(F("<form  \"GET\" ACTION=\"\">")); 
	client->println(F("<table border=1>")); 
	client->println(F("<tr>"));
	client->println(F("	<td colspan=5>Guidance thrusters </td> "));
	client->println(F("	</tr>")) ;
	client->println(F("<tr>"));
	client->println(F("<td>thruster </td><td>PRISE</td><td>SENS</td><td>LIMITE +</td><td>LIMITE -</td>"));
	client->println(F("</tr>"));
	for (byte i = 0 ;i < 6 ; i++){
		client->println(F("<tr>"));
		client->print(F("<td>"));
		client->print(MOT[i].texte );
		client->print(F("</td>"));
		client->print(F("<td><SELECT id=\""));client->print("O_");client->print(MOT[i].id);client->println(F("\"> "));
		for (int j = 0 ; j < 6 ; j++){
			client->print(F("<OPTION VALUE=\"")); client->print(j) ;   client->print(F("\"")); 
			if(MOT[i].plug == j){client->print(F(" SELECTED"));}; client->print(F(">")); 
			client->print(PLUG_TEXT[j]); client->println(F("</OPTION>"));
		}
		client->println(F("</SELECT>"));
		client->println(F("</td> "));
		client->print(F("<td>"));
		client->print(F("<SELECT id=\""));
		client->print("S_");client->print(MOT[i].id);
		client->println(F("\">"));
		client->print(F("<OPTION VALUE=\"1\""));  if(MOT[i].sens ==  1){client->print(F("SELECTED"));}; client->println(F(">+</OPTION>")); 
		client->print(F("<OPTION VALUE=\"-1\"")); if(MOT[i].sens == -1){client->print(F("SELECTED"));}; client->println(F(">-</OPTION>"));
 		client->println(F("</td>")) ;
		client->println(F("<td><INPUT type=text id=\""));
		client->print("Lp_");client->print(MOT[i].id);
		client->print(F("\" value=\"")); client->print(int( MOT[i].limP));  client->print(F("\"></td>"));
		client->print(F("<td><INPUT type=text id=\""));
		client->print("Lm_");client->print(MOT[i].id);
		client->print(F("\" value=\"")); client->print(int( MOT[i].limM));  client->print(F("\"></td>"));
      		client->print(F("</tr>"));
	}
	client->println(F("</table><hr>"));
	client->println(F("<table border=1>")); 
	client->println(F("<td>AXE </td><td>Prop -</td><td>Integral -</td><td>Differentiel -</td>"));
	client->println(F("</tr>"));
	for (byte i = 0 ;i < 3 ; i++){
		client->println(F("<tr>"));
		client->println(F("<td>"));
		client->println(PID[i].texte );
		client->println(F("</td>"));
		client->println(F("<td><INPUT type=text id=\""));
		client->println("P_");client->println(PID[i].id);
		client->print(F("\" value=\""));  client->print(int(PID[i].P));  client->print(F("\"></td>"));
		client->print(F("<td><INPUT type=text id=\""));
		client->println("I_");client->println(PID[i].id);
		client->print(F("\" value=\"")); client->print(int(PID[i].I));   client->print(F("\"></td>"));
		client->print(F("<td><INPUT type=text id=\""));
		client->print("D_");client->println(PID[i].id);
		client->print(F("\" value=\"")); client->print(int(PID[i].D));  client->print(F("\"></td>"));
       		client->println(F("</tr>"));
	}

	client->println(F("</table>"));
	client->println(F("<INPUT type=\"hidden\"  name=\"Param\" value = \"\">"));
	client->println(F("<INPUT  value=\"Envoyer\" onClick=\"toto()\">"));
	client->println(F("</form>")) ;
	print_HTML_END
	delay(1); // give the web browser time to receive the data
	client->stop();
}












#endif

