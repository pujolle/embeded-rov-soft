/*! \file
\brief Class for decoding an storing order send by the PILOT surface station

*/
#ifndef PILOT_COMMAND_H
#define PILOT_COMMAND_H

// Commands from pilot station
/*!
\brief Class for decoding an storing order send by the PILOT surface station

*/
class PILOT_COMMANDS {
public :
	/*! piloting mode -->  0 : manual , 1 : auto depth control ,  2 : angular rate control , 3 : Something else ? */
	char PilotMode ; 
	/*! Yaw, pitch and roll commands from pilot client  : <ul>
	<li>ZERO --> joystick neutral, motor stop or angular rate = 0 
	<li>+90 --> motors to max positive torque (see ROV axis) 
	<li>-90 --> motors to max negative torque (see ROV axis)
	</ul>*/
	char E[3] ; 
	/*! Yaw, pitch and roll trim commands from pilot client */
	char TE[3] ; 
	/*! Speed command along X (longitudinal) Y (lateral) and Z (vertical) */
	char S[3] ; 
	/*! Speed trim command along Y (lateral) and Z (vertical) always 0 on X */
	char TS[3] ; 
	/*! 0 --> 100 % of led power */
	char LedPower ; 
 	/*! 0 --> 179 angle for cam tilt servo */
	char TiltCam ;  
	/*! \todo Mode de fnct de la cam pilote N'A RIEN A FAIRE ICI : NETTOYER */
	char ModeCam ; 
	/*! \todo accelerator and brake Devrait être repris pour être gérer dans le JAVA corriger JAVA et ici*/
	char A , B ;  
	/*! \todo Probablement un résidu des essais ave requette JSON TODO nettoyer*/
	int parse_request_byte(char buffer[]);
	void lire_request(EthernetClient Client) ;
private :
} ;

void  PILOT_COMMANDS::lire_request(EthernetClient Client){
	S[X_axis] = Client.read() ; // 0
	S[Y_axis] = Client.read() ; // 1
	S[Z_axis] = Client.read() ; // 2
	//
	TS[X_axis] = 0 ; // no trim along X axis
	TS[Y_axis] = Client.read() ; // 3
	TS[Z_axis] = Client.read() ; // 4
	//
	LedPower =  Client.read() ; // 5
	TiltCam =  Client.read() ;  // 6
	ModeCam =  Client.read() ;  // 7
	//
	TE[Yaw_axis] = Client.read() ; // 8
	TE[Pitch_axis] = Client.read() ; // 9
	TE[Roll_axis] = Client.read() ; // 10

	PilotMode = Client.read() ; // 11
	//
	E[Yaw_axis] = Client.read() ; // 12
	E[Pitch_axis] = Client.read() ; // 13
	E[Roll_axis] = Client.read() ; // 14
	//
	A = Client.read() ; // 15 acceleration order --> increase the power on main propeller
	B = Client.read() ; // 16 brake order	--> decrease the power on main propeler
	#ifdef DEBUG
//	Serial.print (" 0 Speed X = ") ; Serial.print (int(S[0])) ;
//	Serial.print (" 1 Sy  = ") ; Serial.print (int (S[1])) ;
//	Serial.print (" 2 Sz = ") ; Serial.print (int(S[2])) ;
//	//
//	Serial.print (" 3 Trim Sy = ") ; Serial.print (int(TS[1])) ;
//	Serial.print (" 4 Tsz  = ") ; Serial.print (int (TS[2])) ;
//	//
//	Serial.print (" 5 Led pow = ") ; Serial.print (int(LedPower)) ;
//	Serial.print (" 6 T Cam  = ") ; Serial.print (int (TiltCam)) ;
//	Serial.print (" 7 M Cam = ") ; Serial.print(int(ModeCam)) ;
	//
//	Serial.print (" 8 Trim Yaw = ") ; Serial.print (int(TE[0])) ;
//	Serial.print (" 9 Tp  = ") ; Serial.print (int (TE[1])) ;
//	Serial.print (" 10 Tr = ") ; Serial.print (int(TE[2])) ;
	//
//	Serial.print (" 11 Mode Pilot = ") ; Serial.print (int(PilotMode)) ;
	//
//	Serial.print (" 12 YAW = ") ; Serial.print (int(E[Yaw_axis])) ;
//	Serial.print (" 13 PITCH  = ") ; Serial.print (int (E[Pitch_axis])) ;
//	Serial.print (" 14 ROLL = ") ; Serial.println (int(E[Roll_axis])) ;
	Serial.print (" 15 Accel  = ") ; Serial.print (int (A)) ;
	Serial.print (" 16 Brake = ") ; Serial.println (int(B)) ;
	#endif
}

#endif

